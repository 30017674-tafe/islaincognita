﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float reach = 12f; // interaction distance

    void Update()
    {
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit crosshairUpdate, reach))
        {
            InteractableObject target = crosshairUpdate.transform.GetComponent<InteractableObject>();
            Enemy enemy = crosshairUpdate.transform.GetComponent<Enemy>();
            if (target == null)
            {
                target = crosshairUpdate.transform.GetComponentInParent<InteractableObject>();
            }
            if (enemy == null)
            {
                enemy = crosshairUpdate.transform.GetComponentInParent<Enemy>();
            }
            if (target != null || enemy != null)
            {
                UIManagement.instance.crosshair.sprite = UIManagement.instance.interactableCrosshair;
            }
        }
        else
        {
            UIManagement.instance.crosshair.sprite = UIManagement.instance.baseCrosshair;
        }
        if (Input.GetButtonDown("Use"))
        {
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit, reach))
            {
                Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * reach, Color.magenta, 2.0f);
                InteractableObject obj = hit.transform.GetComponent<InteractableObject>(); // used for interacting with the target object
                if (obj == null)
                {
                    obj = hit.transform.GetComponentInParent<InteractableObject>(); // checks for the interactableobject property in the parent object
                }
                if (obj != null)
                {
                    obj.Activate(); // activates the object
                }
                Enemy enemy = hit.transform.GetComponent<Enemy>();
                if (enemy == null)
                {
                    enemy = hit.transform.GetComponentInParent<Enemy>();
                }
                if (enemy != null)
                {
                    if (GameManager.instance.stunCooldown <= 0)
                    {
                        UIManagement.instance.cooldown.fillAmount = 1;
                        Enemy.enemyInstance.Stun(); // stuns the enemy
                        GameManager.instance.stunCooldown = 7f;
                        UIManagement.instance.StunCooldown();
                    }
                }
            }
        }
    }

   
}
