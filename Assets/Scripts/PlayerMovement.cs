﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement Settings")]
    public bool canMove = true; // will be used for attack state
    public float walkSpeed = 7.5f;
    public float sprintSpeed = 11.5f;
    public float jumpSpeed = 10f;
    public float gravity = 23f;
    [Header("Mouse Settings")]
    public float sensitivity = 2f;
    public float lookCap = 45f;

    Vector3 currentPos = Vector3.zero;
    Vector3 currentDir = Vector3.zero;
    Camera playerCamera;
    CharacterController character;
    Vector3 moveDir = Vector3.zero;
    float rotationY = 0;

    void Start()
    {
        character = GetComponent<CharacterController>(); // used for character movement
        playerCamera = GetComponentInChildren<Camera>(); // used for mouse look
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        canMove = !GameManager.instance.paused;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameManager.instance.paused == true)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
                GameManager.instance.Unpause();
            }
            else
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
                GameManager.instance.Pause();
            }
        }
        
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        Vector3 right = transform.TransformDirection(Vector3.right);
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        float speedZ = canMove ? (isRunning ? sprintSpeed : walkSpeed) * Input.GetAxis("Vertical") : 0; // if the player can move, it checks for whether or not they're running.
        float speedX = canMove ? (isRunning ? sprintSpeed : walkSpeed) * Input.GetAxis("Horizontal") : 0; // if they are, their speed is the sprint speed. if not, it is their walking speed. if they can't move, speed is zero.
        
        if (speedZ != 0)
        {
            speedX = speedX / 1.5f ; // without this, you move twice as fast if you're holding both forward/back and sides
        }

        float moveDirY = moveDir.y;
        moveDir = (forward * speedZ) + (right * speedX);
        if (Input.GetButtonDown("Jump") && canMove && character.isGrounded)
        {
            moveDir.y = jumpSpeed;
        }
        else
        {
            moveDir.y = moveDirY;
        }
        if (!character.isGrounded)
        {
            moveDir.y -= gravity * Time.deltaTime;
        }
        character.Move(moveDir * Time.deltaTime); // applies deltaTime twice, as gravity is an acceleration

        if (canMove) // if the player can move, rotates the player and the camera.
        {
            rotationY += -Input.GetAxis("Mouse Y") * sensitivity;
            rotationY = Mathf.Clamp(rotationY, -lookCap, lookCap);
            playerCamera.transform.localRotation = Quaternion.Euler(rotationY, 0, 0);
            transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * sensitivity, 0);
        }
    }
}
