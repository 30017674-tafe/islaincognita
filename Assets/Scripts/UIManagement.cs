﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagement : MonoBehaviour
{
    public Image enemyAlert;
    public Text timerText;
    public Image crosshair;
    public Sprite interactableCrosshair;
    public Sprite baseCrosshair;
    public Image cooldown;
    public bool countingTime;
    public Image pauseMenu; 

    public static UIManagement instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    private void Start()
    {
        pauseMenu.gameObject.SetActive(false);
        enemyAlert.gameObject.SetActive(false);
        cooldown.fillAmount = 0;
        countingTime = true;
    }

    private void Update()
    {
        StunCooldown();
        
    }
    public void UpdateTimer(float time)
    {
        timerText.text = "Time: " + time.ToString("F1") + "s";
    }

    public void ShowEnemyAlert()
    {
        enemyAlert.gameObject.SetActive(true);
    }

    public void HideEnemyAlert()
    {
        enemyAlert.gameObject.SetActive(false);
    }

    public void StunCooldown()
    {
        cooldown.fillAmount = (GameManager.instance.stunCooldown * 14.2857143f) / 100f;
    }

}
