﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public bool alerted = false;
    public bool objectRetrieved = false;
    public bool altPath = false;
    public bool paused = false;

    public float stunCooldown;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        alerted = false;
    }

    // Start is called before the first frame update
    void Start()
    { 
        objectRetrieved = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (objectRetrieved == true)
        {
            UIManagement.instance.ShowEnemyAlert();
            altPath = true;
            alerted = true;
        }
        if (stunCooldown > 0)
        {
            stunCooldown -= Time.deltaTime;
        }
        if (stunCooldown <= 0)
        {
            stunCooldown = 0;
        }
    }

    public void Pause()
    {
        paused = true;
        Time.timeScale = 0;
        UIManagement.instance.pauseMenu.gameObject.SetActive(true);

    }

    public void Unpause()
    {
        paused = false;
        Time.timeScale = 1;
        UIManagement.instance.pauseMenu.gameObject.SetActive(false);
        
    }
}
