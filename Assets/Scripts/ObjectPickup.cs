﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPickup : InteractableObject
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Activate()
    {
        // play pickup animation
        GameManager.instance.objectRetrieved = true;
        gameObject.SetActive(false);
    }
}
