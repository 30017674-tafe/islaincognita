﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{
    public Animator targetAnim;
    public GameObject teleDest;

    // Start is called before the first frame update
    void Start()
    {
        targetAnim = GetComponentInChildren<Animator>();
        targetAnim.SetBool("DoorShut", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            targetAnim.SetBool("DoorShut", true);
            Enemy.enemyInstance.Teleport(teleDest.transform) ;
        }
    }
}
