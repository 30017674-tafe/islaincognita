﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltPathOpen : MonoBehaviour
{
    void Update()
    {
        gameObject.SetActive(!GameManager.instance.altPath); // opens an alternate path when the target item gets picked up
    }
}
