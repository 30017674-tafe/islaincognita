﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;

public class Enemy : MonoBehaviour
{
    public float viewRadius;
    public float prevRadius;
    public bool alerted = false;

    public static Enemy enemyInstance;

    [SerializeField]
    public AudioSource audioSource;
    public float stoppingDistance;

    // all the enemy states
    public EnemyIdle idleState;
    public EnemyChase chaseState;
    public EnemyPatrol patrolState;
    public EnemyStun stunState;
    public EnemyAttack attackState;

    private readonly FiniteStateMachine stateMachine = new FiniteStateMachine();
    [SerializeField]
    private bool targetInRange = false;

    public NavMeshAgent Agent { get; private set; } 
    public Transform Target { get; private set; }
    public Animator Anim { get; private set; }

    private void Awake()
    {
        Agent = GetComponent<NavMeshAgent>();
        Anim = GetComponentInChildren<Animator>();
        if (enemyInstance == null)
        {
            enemyInstance = this;
        }
    }

    private void Start()
    {
        // creates new instances of the enemy states, as well as getting the player's transform and the agent's stoppingdistance
        Agent.stoppingDistance = stoppingDistance;
        Target = GameObject.FindWithTag("Player").transform;
        idleState = new EnemyIdle(this, idleState);
        chaseState = new EnemyChase(this, chaseState);
        patrolState = new EnemyPatrol(this, patrolState);
        stunState = new EnemyStun(this, stunState);
        attackState = new EnemyAttack(this, attackState);
        stateMachine.SetState(idleState);
    }
    private void Update()
    {
       alerted = GameManager.instance.alerted; // constantly updates the alerted property to match the gamemanager's alerted property
       if (Target != null)
        { 
            // checks whether or not the player is in range
            if (Vector3.Distance(transform.position, Target.position) <= viewRadius)
            {
                targetInRange = true;
            }
            else
            {
                targetInRange = false;
            }
            // if the AI is not currently chasing, stunned or attaccking, and the target is in range, the AI starts chasing the player
            if (stateMachine.CurrentState != chaseState && 
                stateMachine.CurrentState != stunState && 
                stateMachine.CurrentState != attackState &&
                targetInRange == true)
            {
                stateMachine.SetState(chaseState);
            }
        }

       if (viewRadius != 100f)
        {
            prevRadius = viewRadius;
        }
       if(alerted == true) // increases the view radius if the enemy is alerted
        {
            viewRadius = 100f;
        }
       if(alerted == false)
        {
            viewRadius = prevRadius;
        }
        stateMachine.Update();
    }

    private void OnDrawGizmos() // shows the view radius in the scene view
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, viewRadius);
    }

    public void Stun()
    {
        stateMachine.SetState(stunState);
    }
    public void Teleport(Transform destination)
    {
        GetComponent<NavMeshAgent>().gameObject.SetActive(false);
        transform.position = destination.position;
        transform.rotation = destination.rotation;
        stateMachine.SetState(idleState);
        GetComponent<NavMeshAgent>().gameObject.SetActive(true);
    }

    [System.Serializable]
    public class EnemyPatrol : FiniteStateMachine.BehaviourState
    {
        public Transform[] patrolPoints;
        public float patrolSpeed = 3;

        private int currentIndex = -1;

        public EnemyPatrol(Enemy i, EnemyPatrol patrolState) : base(i)
        {
            patrolPoints = patrolState.patrolPoints;
            patrolSpeed = patrolState.patrolSpeed;
        }

        public override void StateEnter()
        {
            instance.Agent.isStopped = false;
            instance.Anim.SetBool("isMoving", true);
            GetNextIndex();
            instance.audioSource.PlayOneShot(AudioManager.instance.patrolAudio);
        }
        public override void StateUpdate()
        {
            Vector3 playerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
            Vector3 targetPos2D = new Vector3(patrolPoints[currentIndex].position.x, 0, patrolPoints[currentIndex].position.z);

            float distance = Vector3.Distance(playerPos2D, targetPos2D);

            if (distance <= instance.Agent.stoppingDistance)
            {
                instance.stateMachine.SetState(instance.idleState);
            }
            else
            {
                Debug.DrawLine(instance.transform.position, patrolPoints[currentIndex].position, Color.cyan);
                instance.Agent.SetDestination(patrolPoints[currentIndex].position);
                instance.Agent.speed = patrolSpeed;
                if (instance.Agent.isStopped == true)
                {
                    instance.Agent.isStopped = false;
                    instance.Anim.SetBool("isMoving", true);
                }
            }
        }

        private void GetNextIndex()
        {
            currentIndex++;
            if (currentIndex >= patrolPoints.Length)
            {
                currentIndex = 0;
            }
        }
    }

    [System.Serializable]
    public class EnemyIdle : FiniteStateMachine.BehaviourState
    {
        public Vector2 waitRange;
        [SerializeField]
        private float time = 0;
        [SerializeField]
        private float timer = -1;

        public EnemyIdle(Enemy i, EnemyIdle idle) : base(i)
        {
            waitRange = idle.waitRange;
        }

        public override void StateEnter()
        {
            instance.audioSource.PlayOneShot(AudioManager.instance.idleAudio);
            instance.Agent.isStopped = true;
            instance.Anim.SetBool("isMoving", false);
            timer = 0;
            time = Random.Range(waitRange.x, waitRange.y);

        }

        public override void StateUpdate()
        {
            if (timer >= 0)
            {
                timer += Time.deltaTime;
                if(timer >= time)
                {
                    timer = -1;
                    instance.stateMachine.SetState(instance.patrolState);
                }
            }
        }
        public override void StateExit()
        {
        }
    }

    [System.Serializable]
    public class EnemyChase : FiniteStateMachine.BehaviourState
    {
        public float chaseSpeed = 5;
        public EnemyChase(Enemy i, EnemyChase chaseState) : base(i)
        {
            ID = EnemyStateID.chase;
            chaseSpeed = chaseState.chaseSpeed;
        }

        public override void StateEnter()
        {
            instance.audioSource.PlayOneShot(AudioManager.instance.chaseAudio);
            UIManagement.instance.ShowEnemyAlert();
            instance.Agent.isStopped = false;
            instance.Agent.speed = chaseSpeed;
            instance.Anim.SetBool("isRunning", true);
        }
        public override void StateExit()
        {
            UIManagement.instance.HideEnemyAlert();
            instance.Anim.SetBool("isRunning", false);
        }
        public override void StateUpdate()
        {
            if (instance.Target != null)
            {
                if (instance.targetInRange == true)
                {
                    Vector3 playerPos2D = new Vector3(instance.transform.position.x, 0, instance.transform.position.z);
                    Vector3 targetPos2D = new Vector3(instance.Target.position.x, 0, instance.Target.position.z);
                    float distance = Vector3.Distance(playerPos2D, targetPos2D);

                    if (distance <= instance.Agent.stoppingDistance)
                    {
                        // go to attack
                        if (instance.Agent.isStopped == false)
                        {
                            instance.Agent.isStopped = true;
                            instance.Anim.SetBool("isRunning", false);
                            instance.Anim.SetBool("isMoving", false);
                        }
                        instance.stateMachine.SetState(instance.attackState);
                    }
                    else
                    {
                        Debug.DrawLine(instance.transform.position, instance.Target.position, Color.cyan);
                        instance.Agent.SetDestination(instance.Target.position);
                        if (instance.Agent.isStopped == true)
                        {
                            instance.Agent.isStopped = false;
                            instance.Anim.SetBool("isRunning", true);
                            instance.Anim.SetBool("isMoving", true);
                        }
                    }
                }
                else if (instance.alerted == false)
                {
                    instance.stateMachine.SetState(instance.patrolState);
                }
            }
        }
    }   

    [System.Serializable]
    public class EnemyStun : FiniteStateMachine.BehaviourState
    {
        public float stunTime = 2;
        private float timer = -1;

        public EnemyStun(Enemy i, EnemyStun stunState) : base(i)
        {
            ID = EnemyStateID.stun;
            stunTime = stunState.stunTime;
        }

        public override void StateUpdate()
        {
            if (timer >= 0)
            {
                timer += Time.deltaTime;
                if (timer >= stunTime)
                {
                    timer = -1;
                    if (instance.alerted == false)
                    {
                        if (instance.targetInRange == true)
                        {
                            instance.stateMachine.SetState(instance.chaseState);
                        }
                        else
                        {
                            instance.stateMachine.SetState(instance.patrolState);
                        }
                    }
                    else
                    {
                        instance.stateMachine.SetState(instance.chaseState);
                    }
                }
            }
        }
        public override void StateEnter()
        {
            instance.audioSource.PlayOneShot(AudioManager.instance.stunAudio);
            timer = 0;
            instance.Agent.isStopped = true;
            instance.Anim.SetBool("isMoving", false);
            instance.Anim.SetBool("isStunned", true);
        }

        public override void StateExit()
        {
            instance.Anim.SetBool("isStunned", false);
        }
    }

    [System.Serializable]
    public class EnemyAttack : FiniteStateMachine.BehaviourState
    {
        public float expireTime = 5;
        private float timer = -1;


        public EnemyAttack(Enemy i, EnemyAttack attackState) : base(i)
        {
            ID = EnemyStateID.attack;
            expireTime = attackState.expireTime;
        }

        public override void StateEnter()
        {
            instance.audioSource.PlayOneShot(AudioManager.instance.attackAudio);
            instance.Agent.isStopped = true;
            instance.Anim.SetBool("isMoving", false);
            instance.Anim.SetBool("isRunning", false);
            instance.Anim.SetBool("isAttacking", true);
            FindObjectOfType<PlayerMovement>().enabled = false;
            timer = 0;
        }

        public override void StateUpdate()
        {
            if (timer >= 0)
            {
                timer += Time.deltaTime;
                if (timer >= expireTime)
                {
                    timer = -1;
                    Cursor.lockState = CursorLockMode.Confined;
                    Cursor.visible = true;
                    UnityEngine.SceneManagement.SceneManager.LoadScene(5);
                }
            }
        }

        public override void StateExit()
        {
            instance.Anim.SetBool("isAttacking", false);
        }
    }
}

public class FiniteStateMachine
{
    public BehaviourState CurrentState { get; private set; }

    public void SetState(BehaviourState newState)
    {
        if(CurrentState != null)
        {
            CurrentState.StateExit();
        }
        CurrentState = newState;
        CurrentState.StateEnter();
    }
    public void Update()
    {
        if(CurrentState != null)
        {
            CurrentState.StateUpdate();
        }
    }

    /// <summary>
    ///  template class for defining new enemy states
    /// </summary>
    public abstract class BehaviourState
    {
        public enum EnemyStateID { idle, patrol, chase, stun, attack}

        protected Enemy instance;
        protected EnemyStateID ID;

        public BehaviourState(Enemy i)
        {
            instance = i;
        }

        public virtual void StateEnter() { }
        public virtual void StateExit() { }

        public abstract void StateUpdate();
    }
}
