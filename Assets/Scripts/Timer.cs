﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public float time;
    // Start is called before the first frame update
    void Start()
    {
        time = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (UIManagement.instance.countingTime == true)
        {
            time += Time.deltaTime;
            UIManagement.instance.UpdateTimer(time);
        }
    }
}
