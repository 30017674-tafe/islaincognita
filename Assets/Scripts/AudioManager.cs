﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public AudioClip idleAudio;
    public AudioClip patrolAudio;
    public AudioClip chaseAudio;
    public AudioClip stunAudio;
    public AudioClip attackAudio;
    public AudioClip bgm;
}
